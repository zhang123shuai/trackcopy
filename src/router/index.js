import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import search from '../views/personInfo/search.vue'
import searchDetails from '../views/personInfo/searchDetails.vue'
import detailsInfo from '../views/personInfo/detailsInfo.vue'

const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function (location) {
  return routerPush.call(this, location).catch(err => { })
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    // redirect: '/index',
    // meta: {
    //   title: '关于'
    // }
  },
  {
    path: '/search',
    component: search,
    name: 'search',
  },
  {
    path: '/searchDetails',
    component: searchDetails,
    name: 'searchDetails',
  },
  {
    path: '/detailsInfo',
    component: detailsInfo,
    name: 'detailsInfo',
  },
]
const router = new VueRouter({
  routes
})

router.beforeEach((to, form, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  } else {
    document.title = '某数据画像' //此处写默认的title
  }
  next()
})

export default router
