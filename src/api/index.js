import axios from '@/lib/axios'
import qs from 'qs'
const access_token = localStorage.getItem('access_token');

// // 平台access_token
// export function tokeninfo(data) {
//     return axios({
//         method: 'post',
//         // 可在此添加请求头
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded',
//             'Authorization': access_token,
//         },
//         url: `${ipConfigPingTai}/sso/oauth2/token`,
//         data:qs.stringify(data)
//     });
// }

// 根据code获取access_token
export function oauth2token(data) {
    return axios({
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${encode('clientId:clientSecret')}`
        },
        url: `${ipConfigPingTai}/sso/oauth2/token`,
        data: qs.stringify(data)
    });
}

// 字符串转base64
function encode(str) {
    // 对字符串进行编码
    var encode = encodeURI(str);
    // 对编码的字符串转化base64
    var base64 = btoa(encode);
    return base64;
}
//手机号码查询
export function ApiDataResourcePhonemohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/phonemohu`,
        data
    });
}
//wid查询
export function ApiDataResourceWidmohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/widmohu`,
        data
    });
}
//wnumber查询
export function ApiDataResourceWnumbermohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/wnumbermohu`,
        data
    });
}
//idcard查询
export function ApiDataResourceIdcardmohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/idcardmohu`,
        data
    });
}
//imei查询
export function ApiDataResourceImeimohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/imeimohu`,
        data
    });
}
//imsi查询
export function ApiDataResourceImsimohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/imsimohu`,
        data
    });
}
//email查询
export function ApiDataResourceEmailmohu(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daas/api/daasDMS/ApiDataResource/emailmohu`,
        data
    });
}
//手机号码查询总数
export function ApiDataResourceCountALL(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/countALL`,
        data
    });
}
//wid查询总数
export function ApiDataResourceWidcount(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/widcount`,
        data
    });
}
//wnumber查询总数
export function ApiDataResourceWnumber(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/wnumbercount`,
        data
    });
}
//idcard查询总数
export function ApiDataResourceIdcardcount(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/idcardcount`,
        data
    });
}
//imei查询总数
export function ApiDataResourceImeicount(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/imeicount`,
        data
    });
}
//imsi查询总数
export function ApiDataResourceImsicount(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/imsicount`,
        data
    });
}
//email查询总数
export function ApiDataResourceEmailcount(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/emailcount`,
        data
    });
}
//设备信息-imei查询
export function mobileDevice(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/mobile_device`,
        data
    });
}
//设备信息-imei查询总数
export function mobileCountALL(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/mobile_countALL`,
        data
    });
}
//轨迹信息-phone查询
export function actionTraj(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/action_traj`,
        data
    });
}
//轨迹信息-phone查询总数
export function trajCountALL(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/traj_countALL`,
        data
    });
}
//好友列表-wnumber查询
export function wechat(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/wechat`,
        data
    });
}
//好友列表-wnumber查询总数
export function wechatCountALL(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/wechat_countALL`,
        data
    });
}
//通联信息-查询
export function tonglian(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/tonglian`,
        data
    });
}
//通联信息-查询总数
export function tonglianCountALL(data) {
    return axios({
        method: 'post',
        // 可在此添加请求头
        headers: {
            'Authorization': access_token,
        },
        url: `/test/daasDMS/ssoapi/ApiDataResource/tonglian_countALL`,
        data
    });
}
// ----------------------------------------
// 人员信息搜索
export function portrait(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/portrait`,
        data
    });
}
// 好友列表
export function friendList(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/friendList`,
        data
    });
}
// 昵称列表
export function getNickname(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getNickname`,
        data
    });
}
// 昵称列表
export function getTerminal(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getTerminal`,
        data
    });
}
// 群聊列表
export function getChatroom(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getChatroom`,
        data
    });
}
// 邮箱
export function getEmail(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getEmail`,
        data
    });
}
// 话单
export function getVoice(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getVoice`,
        data
    });
}
// 短信
export function getSms(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getSms`,
        data
    });
}
// 轨迹
export function getTrack(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getTrack`,
        data
    });
}
// 点位
export function getFenxi(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/adsSerPerson/getFenxi`,
        data
    });
}
// 照片
export function hdfsImagesImages(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/hdfsImages/images`,
        data
    });
}